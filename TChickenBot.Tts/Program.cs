﻿using System;
using System.Speech.Synthesis;

namespace TChickenBot.Tts
{
    static class Program
    {
        public static void Main(string outputText, string culture, int speed)
        {
            using (SpeechSynthesizer synth = new SpeechSynthesizer())
            {
                Console.WriteLine($"Text: {outputText}, culture: {culture}, speed: {speed}");

                synth.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult, 1, new System.Globalization.CultureInfo(culture));
                synth.Rate = speed;

                // Configure the audio output.   
                synth.SetOutputToDefaultAudioDevice();

                // Speak a string.  
                synth.Speak(outputText);
            }
        }
    }
}
