﻿using System;
using TChickenBot.Lib;

namespace TChickenBot
{
    static class Program
    {
        private static bool _keepRunning = true;

        static void Main()
        {
            Console.WriteLine("Tchicken_bot V.0.1.0");
            Console.WriteLine();

            Bot bot = new Bot("Tchicken_bot", Environment.GetEnvironmentVariable("TWITCH_TOKEN"), "just_programming");
            bot.Run();

            bool _isPause = false;

            while (_keepRunning)
            {
                string input = Console.ReadLine();

                if (input == "command-new" && !_isPause)
                {
                    AddCommandDialog();
                }
                if (input == "command-del" && !_isPause)
                {
                    RemoveCommandDialog(bot);
                }
                if (input == "pause")
                {
                    _isPause = true;
                    Console.WriteLine("Pausing command execution!");
                }
                if (input == "unpause")
                {
                    _isPause = false;
                    Console.WriteLine("Resuming command execution!");
                }
                if (input == "exit" && !_isPause)
                {
                    ExitDialog(bot);
                }
            }
        }

        static void AddCommandDialog()
        {
            Console.WriteLine("Command Type: ");

            Console.WriteLine("Unimplemented you piece of shit!");
        }

        static void RemoveCommandDialog(Bot bot)
        {
            Console.WriteLine("Command Type: ");

            Console.WriteLine("Name: ");
            string name = Console.ReadLine();

            bot.RemoveCommand(name);
        }

        static void ExitDialog(Bot bot)
        {
            _keepRunning = false;
            bot.Close();
            Console.WriteLine("Closing Bot!");
        }
    }
}
