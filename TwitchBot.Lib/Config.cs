﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace TChickenBot.Lib
{
    class Config
    {
        public string TtsExeDirectory { get; set; }
        public List<string> TtsLanguages { get; set; }
        public string ChatPlaysConfigPath { get; set; }
        public string ScreamAudioFilePath { get; set; }
        public string AnnoyAudioFilePath { get; set; }
        public List<string> BanLinks { get; set; }

        public Config()
        {
            TtsExeDirectory = "./TChickenBot.Tts.exe";
            TtsLanguages = new List<string>();
            ChatPlaysConfigPath = "./chat_play_conf.json";
            ScreamAudioFilePath = "./scream_sounds/";
            AnnoyAudioFilePath = "./annoy/";

            BanLinks = new List<string>
            {
                "https://clck.ru/R9gQV (bigfollows .com)"
            };
        }

        public void WriteConfig(string path)
        {
            string json = JsonConvert.SerializeObject(this);

            File.WriteAllText(path, json);
        }

        public static Config GetConfig(string path)
        {
            Config config = new Config();

            if (!File.Exists(path))
            {
                config.WriteConfig(path);
            }
            else
            {
                string file = File.ReadAllText(path);

                config = JsonConvert.DeserializeObject<Config>(file);
            }

            return config;
        }
    }
}
