﻿using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib
{
    public abstract class CommandBase : ICommand
    {
        public abstract string Identifier { get; }
        public abstract string Name { get; }
        public abstract string Description { get; }

        public virtual bool CanExecute(TwitchClient client, ChatCommand command)
        {
            return command.CommandText == Identifier;
        }
        public abstract void Execute(TwitchClient client, ChatCommand command);

        public virtual string HelpText()
        {
            return $"Command: \"{Name}\", identifier: \"{Identifier}\", description: \"{Description}\"";
        }
    }
}
