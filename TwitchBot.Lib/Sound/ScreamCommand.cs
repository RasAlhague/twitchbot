﻿using NAudio.Wave;
using System;
using System.IO;
using System.Threading.Tasks;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.Sound
{
    class ScreamCommand : CommandBase
    {
        private readonly string _screamFileFolder;
        private readonly Random _random;
        private bool _isPlaying;

        public override string Identifier => "scream";
        public override string Name => "ScreamSound";
        public override string Description => "Plays a scream sound";

        public ScreamCommand(string screamFileFolder)
        {
            _screamFileFolder = screamFileFolder;
            _random = new Random();
            _isPlaying = false;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            using WaveOutEvent outputDevice = new WaveOutEvent();
            var files = Directory.GetFiles(_screamFileFolder);

            using  AudioFileReader audioFile = new AudioFileReader(files[_random.Next(files.Length)]);

            outputDevice.Volume = 0.30f;

            outputDevice.PlaybackStopped += OnPlaybackStopped;

            outputDevice.Init(audioFile);
            outputDevice.Play();

            _isPlaying = true;

            while(_isPlaying)
            {
                Task.Delay(10);
            }
        }

        private void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            _isPlaying = false;
        }
    }
}
