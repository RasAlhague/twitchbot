﻿using NAudio.Wave;
using System;
using System.IO;
using System.Threading.Tasks;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.Sound
{
    class AnnoyCommand : CommandBase
    {
        private readonly string _annoyFileFolder;
        private readonly Random _random;
        private bool _isPlaying;

        public static bool OneIsRunning { get; private set; }
        public override string Identifier => "annoy";
        public override string Name => "Annoy Sound";
        public override string Description => "Plays an anoying piece of music!";

        public AnnoyCommand(string annoyFileFolder)
        {
            _annoyFileFolder = annoyFileFolder;
            _random = new Random();
            _isPlaying = false;
            OneIsRunning = false;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            if (OneIsRunning)
            {
                return;
            }
            OneIsRunning = true;

            using WaveOutEvent outputDevice = new WaveOutEvent();
            var files = Directory.GetFiles(_annoyFileFolder);

            using AudioFileReader audioFile = new AudioFileReader(files[_random.Next(files.Length)]);

            outputDevice.Volume = 0.50f;

            outputDevice.PlaybackStopped += OnPlaybackStopped;

            outputDevice.Init(audioFile);
            outputDevice.Play();

            _isPlaying = true;

            while (_isPlaying)
            {
                Task.Delay(10);
            }
        }

        private void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            _isPlaying = false;
            OneIsRunning = false;
        }
    }
}
