﻿using System;
using System.Collections.Generic;
using System.Text;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.ChatPlays
{
    class ChatPlaysToggleCommand : CommandBase
    {
        private readonly ChatPlaysConfig _config;

        public override string Identifier => "cp-toggle";
        public override string Name => "ChatPlaysToggle";
        public override string Description => "Enables or disables the chat plays functionality";

        public ChatPlaysToggleCommand(ChatPlaysConfig config)
        {
            _config = config;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            _config.Enabled = !_config.Enabled;

            if (_config.Enabled)
            {
                client.SendMessage(command.ChatMessage.Channel, "Chat plays has been enabled!");
            }
            else
            {
                client.SendMessage(command.ChatMessage.Channel, "Chat plays has been disabled!");
            }
        }

        public override bool CanExecute(TwitchClient client, ChatCommand command)
        {
            return command.CommandText == Identifier && (command.ChatMessage.IsModerator || command.ChatMessage.IsBroadcaster);
        }
    }
}
