﻿using System;
using System.Net.Http;

namespace TChickenBot.Lib.ChatPlays
{
    public class ChatPlaysClient : HttpClient
    {
        public void SendUrl(string url)
        {
            var color = Console.ForegroundColor;
            Console.WriteLine($"Sending: {url}");

            try
            {
                var resp = GetAsync(url).Result;

                if (resp.IsSuccessStatusCode)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Success!");
                    Console.ForegroundColor = color;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Fail!");
                    Console.ForegroundColor = color;
                }
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Fail!");
                Console.ForegroundColor = color;
            }
        }

        public void SendPressKey(VirtualKey key, long duration)
        {
            SendUrl($"/press/{(long)key}/{duration}");
        }

        public void SendLeftMouse(long duration)
        {
            SendUrl($"/left/{duration}");
        }

        public void SendRightMouse(long duration)
        {
            SendUrl($"/right/{duration}");
        }

        public void SendMiddleMouse(long duration)
        {
            SendUrl($"/middle/{duration}");
        }

        public void SendWheelScroll(int scroll)
        {
            SendUrl($"/wheel/{scroll}");
        }

        public void SendMoveAbs(int x, int y)
        {
            SendUrl($"/move/abs/{x}/{y}");
        }

        public void SendMoveRel(int x, int y)
        {
            SendUrl($"/move/rel/{x}/{y}");
        }
    }
}
