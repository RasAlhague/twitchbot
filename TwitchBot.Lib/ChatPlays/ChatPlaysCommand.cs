﻿using System;
using System.Linq;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.ChatPlays
{
    class ChatPlaysCommand : CommandBase
    {
        private readonly ChatPlaysConfig _config;
        private readonly ChatPlaysClient _client;

        public override string Identifier => "bob";
        public override string Name => "ChatPlays";
        public override string Description => "Lets the chat control my game!";

        public ChatPlaysCommand(ChatPlaysConfig config)
        {
            _client = new ChatPlaysClient
            {
                BaseAddress = new Uri("http://localhost:8000")
            };

            _config = config;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            var chatKeyInput = _config.ChatKeyInputs.FirstOrDefault(x => x.Keyword == command.CommandText);
            chatKeyInput?.Run(_client);

            var chatMouseInput = _config.ChatMouseInputs.FirstOrDefault(x => x.Keyword == command.CommandText);
            chatMouseInput?.Run(_client);
        }

        public override bool CanExecute(TwitchClient client, ChatCommand command)
        {
            return _config.Enabled && (_config.ChatMouseInputs.Any(x => x.Keyword == command.CommandText) || _config.ChatKeyInputs.Any(x => x.Keyword == command.CommandText));
        }
    }
}
