﻿using System;
using System.Collections.Generic;
using System.Text;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.ChatPlays
{
    class ChatPlaysReloadConfCommand : CommandBase
    {
        private readonly ChatPlaysConfig _config;
        private readonly string _chatPlaysConfigPath;

        public override string Identifier => "cp-reload";
        public override string Name => "ChatPlaysReload";
        public override string Description => "Reloads the configuration for chat plays";

        public ChatPlaysReloadConfCommand(ChatPlaysConfig config, string chatPlaysConfigPath)
        {
            _config = config;
            _chatPlaysConfigPath = chatPlaysConfigPath;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            if (!_config.Enabled)
            {
                var newConfig = ChatPlaysConfig.LoadConfig(_chatPlaysConfigPath);

                _config.ChatKeyInputs.Clear();
                _config.ChatKeyInputs.AddRange(newConfig.ChatKeyInputs);

                _config.ChatMouseInputs.Clear();
                _config.ChatMouseInputs.AddRange(newConfig.ChatMouseInputs);

                client.SendMessage(command.ChatMessage.Channel, "Chat plays config has been reloaded!");
            }
        }

        public override bool CanExecute(TwitchClient client, ChatCommand command)
        {
            return command.CommandText == Identifier && command.ChatMessage.IsBroadcaster;
        }
    }
}
