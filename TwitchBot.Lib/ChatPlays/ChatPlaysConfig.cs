﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace TChickenBot.Lib.ChatPlays
{
    public class ChatPlaysConfig
    {
        public bool Enabled { get; set; }
        public List<ChatMouseInput> ChatMouseInputs { get; set; }
        public List<ChatKeyInput> ChatKeyInputs { get; set; }

        public ChatPlaysConfig()
        {
            Enabled = false;
            ChatKeyInputs = new List<ChatKeyInput>
            {
                new ChatKeyInput() { VirtualKey = VirtualKey.W, HoldDurationInMs = 1000, Keyword = "vor" }
            };

            ChatMouseInputs = new List<ChatMouseInput>
            {
                new ChatMouseInput() { Action = MouseAction.MoveBy, X = 500, Y = 0, Keyword = "rot-links" }
            };
        }

        public static ChatPlaysConfig LoadConfig(string path)
        {
            ChatPlaysConfig config = new ChatPlaysConfig();

            if (!File.Exists(path))
            {
                string serilizeJson = JsonConvert.SerializeObject(config);
                File.WriteAllText(path, serilizeJson);
            }

            string json = File.ReadAllText(path);
            config = JsonConvert.DeserializeObject<ChatPlaysConfig>(json);

            return config;
        }
    }
}
