﻿using System;
using System.Diagnostics;

namespace TChickenBot.Lib.ChatPlays
{
    public class ChatMouseInput
    {
        public string Keyword { get; set; }
        public MouseAction Action { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public void Run(ChatPlaysClient client)
        {
            Debug.WriteLine($"ChatMouseInput: {Keyword}, MouseAction: {Action}, X: {X}, Y: {Y}");

            switch (Action)
            {
                case MouseAction.LeftClick:
                    client.SendLeftMouse(50);
                    break;
                case MouseAction.RightClick:
                    client.SendRightMouse(50);
                    break;
                default:
                    Console.WriteLine("Unimplemented!");
                    break;
            }
        }
    }
}
