﻿using System.Diagnostics;

namespace TChickenBot.Lib.ChatPlays
{
    public class ChatKeyInput
    {
        public string Keyword { get; set; }
        public VirtualKey VirtualKey { get; set; }
        public int HoldDurationInMs { get; set; }

        public void Run(ChatPlaysClient client)
        {
            Debug.WriteLine($"ChatInput: {Keyword}, VirtualKey: {VirtualKey}, HoldDurationInMs: {HoldDurationInMs}");

            client.SendPressKey(VirtualKey, HoldDurationInMs);
        }
    }
}
