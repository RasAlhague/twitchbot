﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TChickenBot.Lib.ChatPlays
{
    public enum MouseAction
    {
        LeftClick,
        LeftDouble,
        LeftDown,
        LeftUp,
        RightClick,
        RightDouble,
        RightDown,
        RightUp,
        MoveBy
    }
}
