﻿using OBSWebsocketDotNet;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib
{
    public class SlimesCommand : CommandBase
    {
        private readonly OBSWebsocket _obsWebSocket;

        public override string Identifier => "slimes";
        public override string Name => "Slimes";
        public override string Description => "Displays the slime army for a short time.";

        public SlimesCommand(OBSWebsocket obsWebsocket)
        {
            _obsWebSocket = obsWebsocket;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            _obsWebSocket.SetSourceRender("Slimes", true);
            Thread.Sleep(15000);
            _obsWebSocket.SetSourceRender("Slimes", false);
        }

        public override bool CanExecute(TwitchClient client, ChatCommand command)
        {
            return command.CommandText == Identifier;
        }
    }
}
