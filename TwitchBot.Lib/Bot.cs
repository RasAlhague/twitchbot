﻿using OBSWebsocketDotNet;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using TChickenBot.Lib.ChatPlays;
using TChickenBot.Lib.Sound;
using TChickenBot.Lib.Tts;
using TwitchLib.Client;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;
using TwitchLib.Client.Extensions;
using TwitchLib.Client.Models;
using TwitchLib.Communication.Clients;
using TwitchLib.Communication.Models;

namespace TChickenBot.Lib
{
    public class Bot
    {
        private readonly TwitchClient _client;
        private readonly List<ICommand> _commands;
        private readonly Config _config;
        private readonly TtsConfig _ttsConfig;
        private readonly ConcurrentQueue<string> _ttsMessages;
        private readonly ChatPlaysConfig _chatPlayConfig;
        private bool _closing;
        private readonly string _connectChannel;
        private readonly OBSWebsocket _obsSocket;

        public Bot(string username, string token, string connectChannel)
        {
            _commands = new List<ICommand>();

            ConnectionCredentials credentials = new ConnectionCredentials(username, token);
            var clientOptions = new ClientOptions
            {
                MessagesAllowedInPeriod = 750,
                ThrottlingPeriod = TimeSpan.FromSeconds(30),
            };
            WebSocketClient webSocketClient = new WebSocketClient(clientOptions);

            _client = new TwitchClient(webSocketClient);
            // Normal commands
            _client.AddChatCommandIdentifier('-');
            // Chat plays commands
            _client.AddChatCommandIdentifier('!');

            _client.Initialize(credentials, connectChannel);
            AddEventHandlers();

            _config = Config.GetConfig(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "tchicken_config.json"));
            _ttsConfig = new TtsConfig()
            {
                Language = "de-DE",
                Speed = 1,
                Enabled = true,
                UnlimitedPowerEnabled = false,
            };
            _chatPlayConfig = ChatPlaysConfig.LoadConfig(_config.ChatPlaysConfigPath);
            _chatPlayConfig.Enabled = false;

            _ttsMessages = new ConcurrentQueue<string>();

            _closing = false;

            _obsSocket = new OBSWebsocket();
            _obsSocket.Connected += OnSocketConnected;
            _obsSocket.Connected += OnSocketDisconnected;

            _obsSocket.Connect("ws://127.0.0.1:4444", Environment.GetEnvironmentVariable("OBS_PWD"));

            SetDefaultCommands();

            _connectChannel = connectChannel;
        }

        private void OnSocketDisconnected(object sender, EventArgs e)
        {
            Console.WriteLine("Obs Socket Disconneted!");
        }

        private void OnSocketConnected(object sender, EventArgs e)
        {
            Console.WriteLine("Socket Connected!");
        }

        public void Run()
        {
            _client.Connect();

            Task.Run(() => RunTtsQueue());
            Task.Run(() => RunSendInfoLoop());
        }


        public void Close()
        {
            _closing = true;
            _client.Disconnect();
            _obsSocket.Disconnect();
        }

        public bool AddCommand(ICommand command)
        {
            if (_commands.Any(x => x.Name == command.Name))
            {
                return false;
            }

            _commands.Add(command);

            return true;
        }

        public bool RemoveCommand(string name)
        {
            if (!_commands.Any(x => x.Name == name))
            {
                return false;
            }

            _commands.RemoveAll(x => x.Name == name);

            return true;
        }

        private void AutoBan(ChatMessage message)
        {
            if (_config.BanLinks.Any(x => message.Message.ToLower().Contains(x.ToLower())))
            {
                var channel = _client.GetJoinedChannel(_connectChannel);

                _client.BanUser(channel, message.Username, "Wurde gebannt weil er einen spam link gesendet hat.");
                Debug.WriteLine($"banned user: {message.Username}");
            }
        }

        private void RunTtsQueue()
        {
            while (true)
            {
                if (!_ttsConfig.UnlimitedPowerEnabled && _ttsConfig.Enabled && _ttsMessages.TryDequeue(out string message))
                {
                    RunTts(message, _ttsConfig.Language, _ttsConfig.Speed, true);
                }
                if (_closing)
                {
                    return;
                }

                Thread.Sleep(10);
            }
        }

        private void AddEventHandlers()
        {
            _client.OnLog += Client_OnLog;
            _client.OnJoinedChannel += Client_OnJoinedChannel;
            _client.OnMessageReceived += Client_OnMessageReceived;
            _client.OnNewSubscriber += Client_OnNewSubscriber;
            _client.OnConnected += Client_OnConnected;
            _client.OnChatCommandReceived += Client_OnChatCommandReceived;
        }

        private void Client_OnChatCommandReceived(object sender, OnChatCommandReceivedArgs e)
        {
            var executableCommands = _commands.Where(c => c.CanExecute(_client, e.Command));

            foreach (var command in executableCommands)
            {
                Task.Run(() => command.Execute(_client, e.Command));
            }
        }

        private void Client_OnConnected(object sender, OnConnectedArgs e)
        {
            Console.WriteLine($"Connected to {e.AutoJoinChannel}");
        }

        private void Client_OnNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
            if (e.Subscriber.SubscriptionPlan == SubscriptionPlan.Prime)
                _client.SendMessage(e.Channel, $"Why is wasting his prime here? Dont get it as my content is crap... Well thanks anyway... as if that ever would happen xD");
            else
                _client.SendMessage(e.Channel, $"Wieso subscribst du hier {e.Subscriber.DisplayName}? Hier gibt es nur chicken und langweiligen Kram! Ich denke diese Nachricht wird eh niemals wer sehen xD");
        }

        private void Client_OnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            var message = e.ChatMessage.Message;
            bool containsLink = false;

            Regex regex = new Regex(@"https://[\w\d\.]+/[\w\d]+");

            SendPleaseNoTrump(e, message);

            if (regex.IsMatch(message))
            {
                containsLink = true;
            }

            AutoBan(e.ChatMessage);

            if (message.ToUpper().Contains("CHICKEN"))
            {
                _client.SendMessage(e.ChatMessage.Channel, "Wer hat nach mir gefragt? By the way command start with '-' ");
            }

            if (!message.StartsWith("@"))
            {
                RunTtsOnMessageReceived(e, containsLink);
            }
        }

        private void RunTtsOnMessageReceived(OnMessageReceivedArgs e, bool containsLink)
        {
            var executableCommands = _commands.Where(c => e.ChatMessage.Message.StartsWith($"-{c.Identifier}") || e.ChatMessage.Message.StartsWith($"!"));

            string ttsMessage = $"{e.ChatMessage.Username} says: {e.ChatMessage.Message}";

            if (!executableCommands.Any() && _ttsConfig.Enabled && _ttsConfig.UnlimitedPowerEnabled && !containsLink)
            {
                RunTts(ttsMessage, _ttsConfig.Language, _ttsConfig.Speed, false);
            }

            if (!executableCommands.Any() && _ttsConfig.Enabled && !_ttsConfig.UnlimitedPowerEnabled && !containsLink)
            {
                _ttsMessages.Enqueue(ttsMessage);
            }
        }

        private void SendPleaseNoTrump(OnMessageReceivedArgs e, string message)
        {
            if (message.ToLower().Contains("trump"))
            {
                _client.SendMessage(e.ChatMessage.Channel, $"Bitte lasst diesen namen aus dem Spiel! Sonst rufe ich die Zelda Hühnchen!");
            }
        }

        private void Client_OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
            Console.WriteLine("Twitch chicken connected via TwitchLib!");
            _client.SendMessage(e.Channel, "Guten Tag, mein name ist chicken bot und ich bin heute ihr bot zum Streamer ärgern. Have FUN!");
        }

        private void Client_OnLog(object sender, OnLogArgs e)
        {
            Console.WriteLine($"{e.DateTime}: {e.BotUsername} - {e.Data}");
        }

        private void RunTts(string message, string culture, int speed, bool isBlocking)
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo(_config.TtsExeDirectory)
            {
                Arguments = $"--output-text \"{message}\" --culture \"{culture}\" --speed {speed}",
                CreateNoWindow = true
            };

            var ttsProcess = new Process
            {
                StartInfo = processStartInfo
            };

            if (isBlocking)
            {
                ttsProcess.Start();
                ttsProcess.WaitForExit();
            }
            else
            {
                ttsProcess.Start();
            }

        }

        private void SetDefaultCommands()
        {
            // TTS COMMANDS
            _commands.Add(new TtsSetLanguageCommand(_ttsConfig));
            _commands.Add(new TtsSetSpeedCommand(_ttsConfig));
            _commands.Add(new TtsToggleCommand(_ttsConfig));
            _commands.Add(new TtsUnlimitedPowerCommand(_ttsConfig));
            _commands.Add(new StopTtsCommand());

            // CHAT PLAYS COMMANDS
            _commands.Add(new ChatPlaysCommand(_chatPlayConfig));
            _commands.Add(new ChatPlaysReloadConfCommand(_chatPlayConfig, _config.ChatPlaysConfigPath));
            _commands.Add(new ChatPlaysToggleCommand(_chatPlayConfig));

            _commands.Add(new ScreamCommand(_config.ScreamAudioFilePath));
            _commands.Add(new AnnoyCommand(_config.AnnoyAudioFilePath));
            _commands.Add(new SlimesCommand(_obsSocket));
        }

        private void RunSendInfoLoop()
        {
            while (true)
            {
                foreach (var channel in _client.JoinedChannels)
                {
                    _client.SendMessage(channel, "Hallo, welcome to the stream. Please be nice to everyone and if you really wanna spam ask for tts and unlimited power. ");

                    if (_ttsConfig.Enabled)
                    {
                        _client.SendMessage(channel, "All chat messages will be vocalized by me via my own programmed tts function. You can also change the language via '-tts-set-lang <lang>'");
                        _client.SendMessage(channel, "Sometimes a self written chat plays will or can be enabled!'");
                    }
                    else
                    {
                        _client.SendMessage(channel, "If you want tts chat you can ask for it.'");
                    }
                }

                if (_closing)
                {
                    return;
                }

                Thread.Sleep(600000);
            }
        }
    }
}
