﻿using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib
{
    public interface ICommand
    {
        string Identifier { get; }
        string Name { get; }
        string Description { get; }
        void Execute(TwitchClient client, ChatCommand command);
        bool CanExecute(TwitchClient client, ChatCommand command);
        string HelpText();
    }
}
