﻿using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.Tts
{
    public class TtsUnlimitedPowerCommand : CommandBase
    {
        private readonly TtsConfig _ttsConfig;

        public override string Identifier => "tts-unlimited-power";
        public override string Name => "TtsUnlimitedPower";
        public override string Description => "Disables the tts queue.";

        public TtsUnlimitedPowerCommand(TtsConfig ttsConfig)
        {
            _ttsConfig = ttsConfig;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            _ttsConfig.UnlimitedPowerEnabled = !_ttsConfig.UnlimitedPowerEnabled;

            if (_ttsConfig.UnlimitedPowerEnabled)
            {
                client.SendMessage(command.ChatMessage.Channel, "Unlimited power has been enabled!");
            }
            else
            {
                client.SendMessage(command.ChatMessage.Channel, "Unlimited power has been disabled!");
            }
        }

        public override bool CanExecute(TwitchClient client, ChatCommand command)
        {
            return command.CommandText == Identifier && (command.ChatMessage.IsModerator || command.ChatMessage.IsBroadcaster);
        }
    }
}
