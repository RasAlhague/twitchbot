﻿using System;
using System.Diagnostics;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.Tts
{
    public class StopTtsCommand : CommandBase
    {

        public override string Identifier => "stts";
        public override string Name => "StopTts";
        public override string Description => "Stops the execution of tts.";

        public StopTtsCommand()
        {
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            foreach (var process in Process.GetProcessesByName("TChickenBot.Tts"))
            {
                process.Kill();
                Console.WriteLine("Ended all tts commands");
            }
        }

        public override bool CanExecute(TwitchClient client, ChatCommand command)
        {
            return command.CommandText == Identifier && (command.ChatMessage.IsModerator || command.ChatMessage.IsBroadcaster);
        }
    }
}
