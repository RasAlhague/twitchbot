﻿using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.Tts
{
    public class TtsToggleCommand : CommandBase
    {
        private readonly TtsConfig _ttsConfig;

        public override string Identifier => "tts-toggle";
        public override string Name => "TtsToggle";
        public override string Description => "Enabled or disables the tts funktion for all messages. Mods and broadcaster only.";

        public TtsToggleCommand(TtsConfig ttsConfig)
        {
            _ttsConfig = ttsConfig;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            _ttsConfig.Enabled = !_ttsConfig.Enabled;

            if (_ttsConfig.Enabled)
            {
                client.SendMessage(command.ChatMessage.Channel, "TTS has been enabled!");
            }
            else
            {
                client.SendMessage(command.ChatMessage.Channel, "TTS has been disabled!");
            }
        }

        public override bool CanExecute(TwitchClient client, ChatCommand command)
        {
            return command.CommandText == Identifier && (command.ChatMessage.IsModerator || command.ChatMessage.IsBroadcaster);
        }
    }
}
