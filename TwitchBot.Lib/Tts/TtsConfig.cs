﻿namespace TChickenBot.Lib.Tts
{
    public class TtsConfig
    {
        public string Language { get; set; }
        public int Speed { get; set; }
        public bool Enabled { get; set; }
        public bool UnlimitedPowerEnabled { get; set; }
    }
}
