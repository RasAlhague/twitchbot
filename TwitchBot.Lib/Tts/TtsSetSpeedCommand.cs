﻿using System.Linq;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.Tts
{
    public class TtsSetSpeedCommand : CommandBase
    {
        private readonly TtsConfig _ttsConfig;
 
        public override string Identifier => "tts-set-speed";
        public override string Name => "TtsSetSpeed";
        public override string Description => "Lets you set the speed of tts. Must be between -10 and 10.";


        public TtsSetSpeedCommand(TtsConfig ttsConfig)
        {
            _ttsConfig = ttsConfig;
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            if (!command.ArgumentsAsList.Any())
            {
                client.SendMessage(command.ChatMessage.Channel, "Could not set speed!");
                client.SendMessage(command.ChatMessage.Channel, "You did not provide a speed!");
                client.SendMessage(command.ChatMessage.Channel, $"Speed can be set with the following command: \"{command.CommandIdentifier}{Identifier} 0\"");

                return;
            }

            if (int.TryParse(command.ArgumentsAsList[0], out int speed))
            {
                if (speed >= -10 && speed <= 10)
                {
                    _ttsConfig.Speed = speed;
                    client.SendMessage(command.ChatMessage.Channel, $"Set speed to {speed}!");
                }
                else
                {
                    client.SendMessage(command.ChatMessage.Channel, "Speed must be between -10 and 10!");
                }
            }
            else
            {
                client.SendMessage(command.ChatMessage.Channel, "Could not set speed!");
                client.SendMessage(command.ChatMessage.Channel, "Speed can be set with the following command: \"-tts-set-speed 0\"");
            }
        }
    }
}
