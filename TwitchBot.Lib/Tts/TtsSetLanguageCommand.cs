﻿using System;
using System.IO;
using System.Linq;
using TwitchLib.Client;
using TwitchLib.Client.Models;

namespace TChickenBot.Lib.Tts
{
    public class TtsSetLanguageCommand : CommandBase
    {
        private readonly TtsConfig _ttsConfig;
        private readonly Config _config;

        public override string Identifier => "tts-set-lang";
        public override string Name => "TtsSetLanguage";
        public override string Description => "Lets you set the language of for tts. Only supported languages can be set.";

        public TtsSetLanguageCommand(TtsConfig ttsConfig)
        {
            _ttsConfig = ttsConfig;
            _config = Config.GetConfig(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "tchicken_config.json"));
        }

        public override void Execute(TwitchClient client, ChatCommand command)
        {
            if (!command.ArgumentsAsList.Any())
            {
                client.SendMessage(command.ChatMessage.Channel, "Could not set language!");
                client.SendMessage(command.ChatMessage.Channel, "You did not provide a language!");
                client.SendMessage(command.ChatMessage.Channel, $"Language can be set with the following command: \"{command.CommandIdentifier}{Identifier} de-DE\"");

                return;
            }

            var comValue = command.ArgumentsAsList[0];

            if (_config.TtsLanguages.Any(x => x == comValue))
            {
                _ttsConfig.Language = comValue;
                client.SendMessage(command.ChatMessage.Channel, $"New language is {comValue}");
            }
            else
            {
                client.SendMessage(command.ChatMessage.Channel, "Could not set language! The language is not installed!");
                client.SendMessage(command.ChatMessage.Channel, "Installed languages: ");

                foreach (var lang in _config.TtsLanguages)
                {
                    client.SendMessage(command.ChatMessage.Channel, lang);
                }
            }
        }
    }
}
